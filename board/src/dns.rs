use domain::base::iana::{Class, Opcode as DomainOpcode, Rcode};
use domain::base::{
    Message as DomainMessage, MessageBuilder as DomainMessageBuilder, ParsedDname,
    Record as DomainRecord, Rtype, Ttl,
};
use domain::rdata::A;
use embassy_executor::Spawner;
use embassy_net::udp::{PacketMetadata, UdpSocket};
use embassy_net::{Ipv4Address, Stack};
use esp_wifi::wifi::{WifiApDevice, WifiDevice};
use log::{info, warn};

use crate::BOARD_IP;

const DNS_MAX_SIZE: usize = 512;
const DNS_PORT: u16 = 53;
const DNS_TTL: Ttl = Ttl::from_mins(2);
const MDNS_IP: Ipv4Address = Ipv4Address::new(224, 0, 0, 251);
const MDNS_PORT: u16 = 5353;

type StaticDomainName = &'static [&'static [u8]];
static MDNS_DOMAIN_NAMES: &[StaticDomainName] = &[&[b"esp", b"local"], &[b"esp32-c3", b"local"]];

fn match_qname(domain_name: StaticDomainName, qname: &ParsedDname<&[u8]>) -> bool {
    qname.iter().count() == (domain_name.len() + 1)
        && domain_name
            .iter()
            .zip(qname)
            .all(|(l, r)| l == &r.as_slice())
}

fn match_qname_any(domain_names: &[StaticDomainName], qname: &ParsedDname<&[u8]>) -> bool {
    domain_names.iter().any(|d| match_qname(d, qname))
}

#[embassy_executor::task]
pub async fn dns_task(stack: &'static Stack<WifiDevice<'static, WifiApDevice>>) {
    let a_value = A::from_octets(BOARD_IP.0[0], BOARD_IP.0[1], BOARD_IP.0[2], BOARD_IP.0[3]);

    let mut rx_buffer = [0; 4096];
    let mut tx_buffer = [0; 4096];
    let mut rx_meta = [PacketMetadata::EMPTY; 16];
    let mut tx_meta = [PacketMetadata::EMPTY; 16];

    let mut recv_buf = [0; DNS_MAX_SIZE];
    let mut socket = UdpSocket::new(
        stack,
        &mut rx_meta,
        &mut rx_buffer,
        &mut tx_meta,
        &mut tx_buffer,
    );
    socket.bind(DNS_PORT).unwrap();

    loop {
        let (n, endpoint) = socket.recv_from(&mut recv_buf).await.unwrap();
        info!("DNS: Received {n} bytes from {endpoint}");
        let query_bytes = &recv_buf[..n];
        match DomainMessage::from_slice(query_bytes) {
            Err(e) => warn!("DNS: Failure to parse message: {e}"),
            Ok(m) => {
                if m.header().opcode() != DomainOpcode::Query {
                    warn!("DNS: wrong query opcode: {}", m.header().opcode());
                    continue;
                }
                let response_bytes: octseq::Array<DNS_MAX_SIZE> = octseq::Array::new();
                let response_builder = DomainMessageBuilder::from_target(response_bytes).unwrap();

                match response_builder.start_answer(m, Rcode::NoError) {
                    Err(e) => warn!("DNS: failure to start the answer: {e}"),
                    Ok(mut response_builder) => {
                        response_builder.header_mut().set_aa(true);
                        let mut answer_count = 0;
                        for question in m.question().flatten() {
                            if question.qtype() != Rtype::A {
                                info!(
                                    "DNS: question with type != A: {}, {}",
                                    question.qtype(),
                                    question.qname()
                                );
                            } else {
                                info!("DNS: answering with board ip for {}", question.qname());
                                let record = DomainRecord::new(
                                    question.qname(),
                                    Class::In,
                                    DNS_TTL,
                                    a_value.clone(),
                                );
                                let _ = response_builder.push(record);
                                answer_count += 1;
                            }
                        }
                        let bytes = response_builder.finish();
                        info!("DNS: Sending response with {answer_count} answers to {endpoint}");
                        let _ = socket.send_to(bytes.as_slice(), endpoint).await;
                    }
                }
            }
        };
    }
}

#[embassy_executor::task]
pub async fn mdns_task(stack: &'static Stack<WifiDevice<'static, WifiApDevice>>) {
    let _ = stack.join_multicast_group(MDNS_IP).await;

    let a_value = A::from_octets(BOARD_IP.0[0], BOARD_IP.0[1], BOARD_IP.0[2], BOARD_IP.0[3]);

    let mut rx_buffer = [0; 4096];
    let mut tx_buffer = [0; 0];
    let mut rx_meta = [PacketMetadata::EMPTY; 16];
    let mut tx_meta = [PacketMetadata::EMPTY; 0];

    let mut recv_buf = [0; DNS_MAX_SIZE];
    let mut socket = UdpSocket::new(
        stack,
        &mut rx_meta,
        &mut rx_buffer,
        &mut tx_meta,
        &mut tx_buffer,
    );
    socket.bind((MDNS_IP, MDNS_PORT)).unwrap();

    let mut rx_buffer = [0; 0];
    let mut tx_buffer = [0; 4096];
    let mut rx_meta = [PacketMetadata::EMPTY; 0];
    let mut tx_meta = [PacketMetadata::EMPTY; 16];

    let mut send_socket = UdpSocket::new(
        stack,
        &mut rx_meta,
        &mut rx_buffer,
        &mut tx_meta,
        &mut tx_buffer,
    );
    send_socket.set_hop_limit(Some(255));
    send_socket.bind((BOARD_IP, MDNS_PORT)).unwrap();

    loop {
        let (n, endpoint) = socket.recv_from(&mut recv_buf).await.unwrap();
        info!("MDNS: Received {n} bytes from {endpoint}");
        let query_bytes = &recv_buf[..n];
        match DomainMessage::from_slice(query_bytes) {
            Err(e) => warn!("MDNS: Failure to parse message: {e}"),
            Ok(m) => {
                if m.header().opcode() != DomainOpcode::Query {
                    warn!("MDNS: wrong query opcode: {}", m.header().opcode());
                    continue;
                }
                let response_bytes: octseq::Array<DNS_MAX_SIZE> = octseq::Array::new();
                let mut response_builder =
                    DomainMessageBuilder::from_target(response_bytes).unwrap();
                {
                    let header = response_builder.header_mut();
                    header.set_id(m.header().id());
                    header.set_aa(true);
                    header.set_qr(true);
                    header.set_opcode(m.header().opcode());
                    header.set_rcode(Rcode::NoError);
                }

                let mut answer_builder = response_builder.answer();
                answer_builder.header_mut().set_aa(true);
                let mut answer_count = 0;
                for question in m.question().flatten() {
                    if !match_qname_any(MDNS_DOMAIN_NAMES, question.qname()) {
                        info!(
                            "MDNS: question with domain_name not in list: {}",
                            question.qname()
                        );
                    } else if question.qtype() != Rtype::A {
                        info!(
                            "MDNS: question with type != A: {}, {}",
                            question.qtype(),
                            question.qname()
                        );
                    } else {
                        info!("MDNS: answering with board ip for {}", question.qname());
                        let record = DomainRecord::new(
                            question.qname(),
                            Class::In,
                            DNS_TTL,
                            a_value.clone(),
                        );
                        let _ = answer_builder.push(record);
                        answer_count += 1;
                    }
                }
                if answer_count == 0 {
                    info!("MDNS: 0 answers, no response sent");
                } else {
                    let bytes = answer_builder.finish();
                    if endpoint.port != MDNS_PORT {
                        info!("MDNS: Sending response with {answer_count} answers to {endpoint}");
                        let _ = send_socket.send_to(bytes.as_slice(), endpoint).await;
                    } else {
                        info!("MDNS: Sending response with {answer_count} answers to mDNS address");
                        let _ = send_socket
                            .send_to(bytes.as_slice(), (MDNS_IP, MDNS_PORT))
                            .await;
                    }
                }
            }
        };
    }
}

pub async fn serve(spawner: &Spawner, stack: &'static Stack<WifiDevice<'static, WifiApDevice>>) {
    spawner.must_spawn(dns_task(stack));
    spawner.must_spawn(mdns_task(stack));
}
