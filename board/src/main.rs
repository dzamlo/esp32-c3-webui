#![no_std]
#![no_main]
#![feature(async_closure)]
#![feature(type_alias_impl_trait)]
#![warn(clippy::pedantic)]
#![allow(clippy::cast_lossless)]
#![allow(clippy::if_not_else)]
#![allow(clippy::no_effect_underscore_binding)]
#![allow(clippy::unused_async)]

use core::sync::atomic::AtomicBool;

use embassy_executor::Spawner;
use embassy_net::{Config, Ipv4Address, Ipv4Cidr, Stack, StackResources, StaticConfigV4};
use embassy_sync::blocking_mutex::raw::CriticalSectionRawMutex;
use embassy_sync::mutex::Mutex;
use embassy_sync::pubsub::PubSubChannel;
use embassy_time::{Duration, Timer};
use embedded_hal_async::digital::Wait;
use embedded_svc::wifi::{AccessPointConfiguration, Configuration, Wifi};
use esp_hal_smartled::{smartLedBuffer, SmartLedsAdapter};
use esp_println::logger::init_logger_from_env;
use esp_wifi::wifi::{WifiApDevice, WifiController, WifiDevice, WifiEvent, WifiState};
use esp_wifi::{initialize, EspWifiInitFor};
use hal::adc::{AdcCalCurve, AdcConfig, Attenuation, ADC};
use hal::clock::ClockControl;
use hal::gpio::{AnyPin, Input, PullDown};
use hal::i2c::I2C;
use hal::peripherals::{Peripherals, ADC1};
use hal::prelude::*;
use hal::timer::TimerGroup;
use hal::{embassy, Rmt, Rng, IO};
use heapless::Vec;
use http::{AppState, WEB_TASK_POOL_SIZE};
use log::info;
use static_cell::make_static;
use {esp_backtrace as _, esp_println as _};

mod dhcp_server;
mod dns;
mod http;

const BOARD_IP: Ipv4Address = Ipv4Address::new(192, 168, 42, 1);
const NUMBER_OF_SOCKETS: usize = WEB_TASK_POOL_SIZE + 5; // 2 for DHCP, 1 for DNS, 2 for mNDS

static BUTTON_STATE_CHANNEL: PubSubChannel<
    CriticalSectionRawMutex,
    bool,
    1,
    WEB_TASK_POOL_SIZE,
    0,
> = PubSubChannel::new();
static BUTTON_STATE: AtomicBool = AtomicBool::new(false);

#[main]
async fn main(spawner: Spawner) {
    init_logger_from_env();
    let peripherals = Peripherals::take();
    let system = peripherals.SYSTEM.split();
    let clocks = ClockControl::max(system.clock_control).freeze();
    let rng = Rng::new(peripherals.RNG);

    let timer_group0 = TimerGroup::new(peripherals.TIMG0, &clocks);
    embassy::init(&clocks, timer_group0);

    let ip_config = Config::ipv4_static(StaticConfigV4 {
        address: Ipv4Cidr::new(BOARD_IP, 24),
        gateway: None,
        dns_servers: Vec::default(),
    });

    let timer = hal::systimer::SystemTimer::new(peripherals.SYSTIMER).alarm0;
    let init = initialize(
        EspWifiInitFor::Wifi,
        timer,
        rng,
        system.radio_clock_control,
        &clocks,
    )
    .unwrap();

    let wifi = peripherals.WIFI;
    let (wifi_interface, controller) =
        esp_wifi::wifi::new_with_mode(&init, wifi, WifiApDevice).unwrap();

    let seed = 424_242;

    let stack = &*make_static!(Stack::new(
        wifi_interface,
        ip_config,
        make_static!(StackResources::<NUMBER_OF_SOCKETS>::new()),
        seed
    ));

    spawner.must_spawn(connection(controller));
    spawner.must_spawn(net_task(stack));

    let io = IO::new(peripherals.GPIO, peripherals.IO_MUX);
    let led = io.pins.gpio7.into_push_pull_output().degrade();
    let button = io.pins.gpio9.into_pull_down_input().degrade();
    let rmt = Rmt::new(peripherals.RMT, 80u32.MHz(), &clocks).unwrap();
    let rmt_buffer = smartLedBuffer!(1);
    let rgb_led = SmartLedsAdapter::new(rmt.channel0, io.pins.gpio2, rmt_buffer);
    let i2c0 = I2C::new(
        peripherals.I2C0,
        io.pins.gpio10,
        io.pins.gpio8,
        400u32.kHz(),
        &clocks,
    );
    let mut adc1_config = AdcConfig::new();
    let atten = Attenuation::Attenuation11dB;
    let adc_pin0 =
        adc1_config.enable_pin_with_cal::<_, AdcCalCurve<ADC1>>(io.pins.gpio3.into_analog(), atten);
    let adc_pin1 =
        adc1_config.enable_pin_with_cal::<_, AdcCalCurve<ADC1>>(io.pins.gpio4.into_analog(), atten);
    let adc1 = ADC::<ADC1>::adc(peripherals.ADC1, adc1_config).unwrap();

    spawner.must_spawn(button_task(button));

    let state = AppState {
        led: make_static!(Mutex::new(led)),
        rng,
        rgb_led: make_static!(Mutex::new(rgb_led)),
        i2c0: make_static!(Mutex::new(i2c0)),
        adc1: make_static!(Mutex::new(adc1)),
        adc_pin0: make_static!(Mutex::new(adc_pin0)),
        adc_pin1: make_static!(Mutex::new(adc_pin1)),
    };
    http::serve(&spawner, stack, state).await;

    dns::serve(&spawner, stack).await;

    dhcp_server::serve(&spawner, stack).await;
}

#[embassy_executor::task]
async fn connection(mut controller: WifiController<'static>) {
    loop {
        if esp_wifi::wifi::get_wifi_state() == WifiState::ApStarted {
            // wait until we're no longer connected
            controller.wait_for_event(WifiEvent::ApStop).await;
            Timer::after(Duration::from_millis(5000)).await;
        }
        if !matches!(controller.is_started(), Ok(true)) {
            let client_config = Configuration::AccessPoint(AccessPointConfiguration {
                ssid: "esp-wifi".try_into().unwrap(),
                ..Default::default()
            });
            controller.set_configuration(&client_config).unwrap();
            controller.start().await.unwrap();
            info!("Wifi started!");
        }
    }
}

#[embassy_executor::task]
async fn net_task(stack: &'static Stack<WifiDevice<'static, WifiApDevice>>) {
    stack.run().await;
}

#[embassy_executor::task]
async fn button_task(mut button: AnyPin<Input<PullDown>>) {
    let publisher = BUTTON_STATE_CHANNEL.immediate_publisher();
    loop {
        BUTTON_STATE.store(false, core::sync::atomic::Ordering::Relaxed);
        publisher.publish_immediate(false);
        let _ = button.wait_for_low().await;
        BUTTON_STATE.store(true, core::sync::atomic::Ordering::Relaxed);
        publisher.publish_immediate(true);
        let _ = button.wait_for_high().await;
    }
}
