use common::{
    ButtonEvent, Empty, GenRandomAnswer, PingAnswer, PingParams, ReadAdcAnswer, ReadAdcParams,
    ReadTemepratureAndHumidityAnswer, SetRgbLedParams,
};
use embassy_executor::Spawner;
use embassy_futures::yield_now;
use embassy_net::Stack;
use embassy_sync::blocking_mutex::raw::CriticalSectionRawMutex;
use embassy_sync::mutex::Mutex;
use embassy_time::{Duration, Timer};
use esp_hal_smartled::SmartLedsAdapter;
use esp_wifi::wifi::{WifiApDevice, WifiDevice};
use hal::adc::{AdcCalCurve, AdcPin, ADC};
use hal::gpio::{Analog, AnyPin, GpioPin, Output, PushPull};
use hal::i2c::I2C;
use hal::peripherals::{ADC1, I2C0};
use hal::prelude::*;
use hal::Rng;
use picoserve::extract::{FromRequest, State};
use picoserve::request::Request;
use picoserve::response::sse::{EventSource, EventWriter};
use picoserve::response::status::{INTERNAL_SERVER_ERROR, NOT_FOUND};
use picoserve::response::{EventStream, File, IntoResponse, Json as JsonResponse};
use picoserve::routing::{get, post};
use serde_json_core::de::Error as JsonError;
use smart_leds::{SmartLedsWrite, RGB};
use static_cell::make_static;

use crate::{BUTTON_STATE, BUTTON_STATE_CHANNEL};

// A lot of the code comes from
// https://github.com/sammhicks/picoserve/blob/main/examples/hello_world_embassy/src/main.rs

const SSE_KEEP_ALIVE_INTERVAL: Duration = Duration::from_secs(5);
pub const WEB_TASK_POOL_SIZE: usize = 8;

struct JsonRejection(JsonError);

impl IntoResponse for JsonRejection {
    async fn write_to<W: picoserve::response::ResponseWriter>(
        self,
        response_writer: W,
    ) -> Result<picoserve::ResponseSent, W::Error> {
        (
            picoserve::response::status::BAD_REQUEST,
            format_args!("Request Body is not valid JSON: {}", self.0),
        )
            .write_to(response_writer)
            .await
    }
}

struct Json<T: serde::de::DeserializeOwned>(pub T);

impl<State, T: serde::de::DeserializeOwned> FromRequest<State> for Json<T> {
    type Rejection = JsonRejection;

    async fn from_request(_state: &State, request: &Request<'_>) -> Result<Json<T>, JsonRejection> {
        serde_json_core::from_slice(request.body())
            .map(|(v, _)| Self(v))
            .map_err(JsonRejection)
    }
}

type StaticMutex<T> = &'static Mutex<CriticalSectionRawMutex, T>;

#[derive(Clone)]
pub struct AppState {
    pub led: StaticMutex<AnyPin<Output<PushPull>>>,
    pub rng: Rng,
    pub rgb_led: StaticMutex<SmartLedsAdapter<hal::rmt::Channel<0>, 0, 25>>,
    pub i2c0: StaticMutex<I2C<'static, I2C0>>,
    pub adc1: StaticMutex<ADC<'static, ADC1>>,
    pub adc_pin0: StaticMutex<AdcPin<GpioPin<Analog, 3>, ADC1, AdcCalCurve<ADC1>>>,
    pub adc_pin1: StaticMutex<AdcPin<GpioPin<Analog, 4>, ADC1, AdcCalCurve<ADC1>>>,
}

struct EmbassyTimer;

impl picoserve::Timer for EmbassyTimer {
    type Duration = embassy_time::Duration;
    type TimeoutError = embassy_time::TimeoutError;

    async fn run_with_timeout<F: core::future::Future>(
        &mut self,
        duration: Self::Duration,
        future: F,
    ) -> Result<F::Output, Self::TimeoutError> {
        embassy_time::with_timeout(duration, future).await
    }
}

struct ButtonEvents;

async fn send_pressed<W: picoserve::io::Write>(
    writer: &mut EventWriter<W>,
    pressed: bool,
) -> Result<(), W::Error> {
    let event = ButtonEvent { pressed };
    let string = serde_json_core::to_string::<_, 32>(&event).unwrap();
    writer.write_event("button_event", string.as_str()).await?;
    Ok(())
}

impl EventSource for ButtonEvents {
    async fn write_events<W: picoserve::io::Write>(
        self,
        mut writer: EventWriter<W>,
    ) -> Result<(), W::Error> {
        if let Ok(mut sub) = BUTTON_STATE_CHANNEL.subscriber() {
            send_pressed(
                &mut writer,
                BUTTON_STATE.load(core::sync::atomic::Ordering::Relaxed),
            )
            .await?;
            loop {
                match embassy_time::with_timeout(SSE_KEEP_ALIVE_INTERVAL, sub.next_message_pure())
                    .await
                {
                    Ok(pressed) => {
                        send_pressed(&mut writer, pressed).await?;
                    }
                    Err(_) => {
                        writer.write_keepalive().await?;
                    }
                }
            }
        }
        Ok(())
    }
}

async fn handler_gen_random(State(mut state): State<AppState>) -> impl IntoResponse {
    let value = state.rng.random();
    JsonResponse(GenRandomAnswer { value })
}

async fn handler_toggle_led(State(state): State<AppState>) -> impl IntoResponse {
    state.led.lock().await.toggle().unwrap();
    JsonResponse(Empty {})
}

async fn handler_ping(Json(params): Json<PingParams>) -> impl IntoResponse {
    JsonResponse(PingAnswer {
        pong_value: params.ping_value,
    })
}

async fn handler_read_adc(
    State(state): State<AppState>,
    Json(params): Json<ReadAdcParams>,
) -> impl IntoResponse {
    let mut adc1 = state.adc1.lock().await;

    macro_rules! block {
        ($e:expr) => {
            loop {
                match $e {
                    Err(_) => yield_now().await,
                    Ok(x) => break x,
                }
            }
        };
    }
    let maybe_adc_value = match params.adc_channel_index {
        0 => {
            let mut pin = state.adc_pin0.lock().await;
            Some(block!(adc1.read(&mut pin)))
        }
        1 => {
            let mut pin = state.adc_pin1.lock().await;
            Some(block!(adc1.read(&mut pin)))
        }
        _ => None,
    };

    match maybe_adc_value {
        Some(adc_value) => Ok(JsonResponse(ReadAdcAnswer { adc_value })),
        None => Err((NOT_FOUND, "Adc channel requested not found")),
    }
}

async fn read_temp_and_rh<T: _esp_hal_i2c_Instance>(
    i2c: &mut I2C<'_, T>,
) -> Result<(f32, f32), ()> {
    const SHTC3_ADDRESS: u8 = 0x70;
    #[allow(clippy::cast_precision_loss)]
    const DIVISOR: f32 = (1 << 16) as f32;

    if i2c.write(SHTC3_ADDRESS, &[0x35, 0x17]).is_err() {
        return Err(());
    }

    Timer::after_millis(1).await;

    if i2c.write(SHTC3_ADDRESS, &[0x78, 0x66]).is_err() {
        return Err(());
    }

    Timer::after_millis(13).await;

    let mut read_buf = [0u8; 6];

    if i2c.read(SHTC3_ADDRESS, &mut read_buf[..]).is_err() {
        return Err(());
    }

    let raw_temp = ((read_buf[0] as u16) << 8) | read_buf[1] as u16;
    let temperature = -45.0 + 175.0 * raw_temp as f32 / DIVISOR;

    let raw_humidity = ((read_buf[3] as u16) << 8) | read_buf[4] as u16;
    let humidity = 100.0 * raw_humidity as f32 / DIVISOR;

    Ok((temperature, humidity))
}

async fn handler_read_temperature_and_humidity(State(state): State<AppState>) -> impl IntoResponse {
    let mut i2c0 = state.i2c0.lock().await;
    match read_temp_and_rh(&mut i2c0).await {
        Ok((temperature, humidity)) => Ok(JsonResponse(ReadTemepratureAndHumidityAnswer {
            temperature,
            humidity,
        })),
        Err(()) => Err((INTERNAL_SERVER_ERROR, "Error while reading the I2C bus.")),
    }
}

async fn handler_set_rgb_led(
    State(state): State<AppState>,
    Json(params): Json<SetRgbLedParams>,
) -> impl IntoResponse {
    let mut rgb_led = state.rgb_led.lock().await;
    let _ = rgb_led.write([RGB::new(params.r, params.g, params.b)].into_iter());
    JsonResponse(Empty {})
}

const JS_FILE: File = File::with_content_type(
    "application/javascript; charset=utf-8",
    include_bytes!("../../frontend/dist/frontend.js.gz"),
);

async fn handler_js() -> impl IntoResponse {
    (("Content-Encoding", "gzip"), JS_FILE)
}

const WASM_FILE: File = File::with_content_type(
    "application/wasm",
    include_bytes!("../../frontend/dist/frontend_bg.wasm.gz"),
);

async fn handler_wasm() -> impl IntoResponse {
    (("Content-Encoding", "gzip"), WASM_FILE)
}

type AppRouter = impl picoserve::routing::PathRouter<AppState>;

fn make_app() -> picoserve::Router<AppRouter, AppState> {
    picoserve::Router::new()
        .route(
            "/",
            get(|| async { File::html(include_str!("../../frontend/dist/index.html")) }),
        )
        .route("/frontend.js", get(handler_js))
        .route("/frontend_bg.wasm", get(handler_wasm))
        .route("/api/button_events", get(move || EventStream(ButtonEvents)))
        .route("/api/gen_random", post(handler_gen_random))
        .route("/api/ping", post(handler_ping))
        .route("/api/toggle_led", post(handler_toggle_led))
        .route("/api/read_adc", post(handler_read_adc))
        .route(
            "/api/read_temperature_and_humidity",
            post(handler_read_temperature_and_humidity),
        )
        .route("/api/set_rgb_led", post(handler_set_rgb_led))
}

pub async fn serve(
    spawner: &Spawner,
    stack: &'static Stack<WifiDevice<'static, WifiApDevice>>,
    state: AppState,
) {
    let app = make_static!(make_app());
    let config = make_static!(picoserve::Config::new(picoserve::Timeouts {
        start_read_request: Some(Duration::from_secs(5)),
        read_request: Some(Duration::from_secs(1)),
        write: Some(Duration::from_secs(1)),
    })
    .keep_connection_alive());

    for id in 0..WEB_TASK_POOL_SIZE {
        spawner.must_spawn(web_task(id, stack, app, config, state.clone()));
    }
}

#[embassy_executor::task(pool_size = WEB_TASK_POOL_SIZE)]
async fn web_task(
    id: usize,
    stack: &'static Stack<WifiDevice<'static, WifiApDevice>>,
    app: &'static picoserve::Router<AppRouter, AppState>,
    config: &'static picoserve::Config<Duration>,
    state: AppState,
) -> ! {
    let mut rx_buffer = [0; 1024];
    let mut tx_buffer = [0; 8192];

    loop {
        let mut socket = embassy_net::tcp::TcpSocket::new(stack, &mut rx_buffer, &mut tx_buffer);

        log::info!("HTTP({id}): Listening on TCP:80...");
        if let Err(e) = socket.accept(80).await {
            log::warn!("HTTP({id}): accept error: {:?}", e);
            continue;
        }

        log::info!(
            "HTTP({id}): Received connection from {:?}",
            socket.remote_endpoint()
        );

        let (socket_rx, socket_tx) = socket.split();

        match picoserve::serve_with_state(
            app,
            EmbassyTimer,
            config,
            &mut [0; 2048],
            socket_rx,
            socket_tx,
            &state,
        )
        .await
        {
            Ok(handled_requests_count) => {
                log::info!(
                    "HTTP({id}): {handled_requests_count} requests handled from {:?}",
                    socket.remote_endpoint()
                );
            }
            Err(err) => log::error!("{err:?}"),
        }
    }
}
