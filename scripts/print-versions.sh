#!/bin/sh

which cargo
cargo --version
which rustc
rustc --version
which make
make --version
which cc
cc --version
which espflash 
espflash --version
which leptosfmt
leptosfmt --version
which trunk
trunk --version
which wasm-bindgen
wasm-bindgen --version
which wasm-opt
wasm-opt --version
true
