#![no_std]
#![warn(clippy::pedantic)]

use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Empty {}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct ButtonEvent {
    pub pressed: bool,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct GenRandomAnswer {
    pub value: u32,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct PingParams {
    pub ping_value: u32,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct PingAnswer {
    pub pong_value: u32,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct ReadAdcParams {
    pub adc_channel_index: u16,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct ReadAdcAnswer {
    pub adc_value: u16,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct ReadTemepratureAndHumidityAnswer {
    pub temperature: f32,
    pub humidity: f32,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct SetRgbLedParams {
    pub r: u8,
    pub g: u8,
    pub b: u8,
}
