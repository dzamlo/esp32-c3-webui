# ESP32-C3 Web Interface

An example of a web interface that run on a ESP32-C3-DevKit-RUST-1.

To run it:
```
cargo install espflash trunk
make run
```

Then connect to the `esp-wifi` Wi-Fi network. Then open you browser and open 
http://esp.local. Then wait for the page to load (this can take a few seconds).
If that doesn't work, you can try http://192.168.42.1

## Pre-commit hook

There is a pre-commit hook to ensure than files are formatted correctly, there
are no warnings and that the binary builds:
```
cargo install leptosfmt
make instal-pre-commit-hook
```

## License

Licensed under either of

 * Apache License, Version 2.0, ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
 * MIT license ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.

## Contribution

Unless you explicitly state otherwise, any contribution intentionally
submitted for inclusion in the work by you, as defined in the Apache-2.0
license, shall be dual licensed as above, without any additional terms or
conditions.
