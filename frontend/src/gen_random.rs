use common::{Empty, GenRandomAnswer};
use leptos::error::Result;
use leptos::{component, create_action, create_signal, view, ErrorBoundary, IntoView, Show};

use crate::utils::{errors_fallback, post_json, Error, Loading};

async fn gen_random() -> Result<GenRandomAnswer> {
    post_json("/api/gen_random", &Empty {}).await
}

#[component]
pub fn GenRandom() -> impl IntoView {
    let (clicked, set_clicked) = create_signal(false);
    let gen_random_action = create_action(move |(): &()| {
        set_clicked(true);
        gen_random()
    });
    let pending = gen_random_action.pending();
    let value = gen_random_action.value();
    let result = move || value().unwrap_or(Err(Error::Unknown.into()));
    let result_view = move || {
        result().map(|answer| {
            view! { <p>Random value: {answer.value}</p> }
        })
    };

    let on_click = move |_| {
        gen_random_action.dispatch(());
    };

    view! {
        <div id="tool-gen-random" class="tool">
            <h2>Random number</h2>
            <button type="button" on:click=on_click>
                Generate a random number
            </button>
            <Show when=move || { !pending() } fallback=|| view! { <Loading/> }>
                <Show when=clicked>
                    <ErrorBoundary fallback=errors_fallback>{result_view}</ErrorBoundary>
                </Show>
            </Show>
        </div>
    }
}
