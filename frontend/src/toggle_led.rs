use common::Empty;
use leptos::error::Result;
use leptos::{component, create_action, create_signal, view, ErrorBoundary, IntoView, Show};
use log::debug;

use crate::utils::{errors_fallback, post_json, Error, Loading};

async fn toggle_the_led() -> Result<Empty> {
    post_json("/api/toggle_led", &Empty {}).await
}

#[component]
pub fn ToggleLed() -> impl IntoView {
    let (clicked, set_clicked) = create_signal(false);
    let toggle_the_led_action = create_action(move |(): &()| {
        set_clicked(true);
        toggle_the_led()
    });
    let pending = toggle_the_led_action.pending();
    let value = toggle_the_led_action.value();
    let result = move || value().unwrap_or(Err(Error::Unknown.into()));
    let result_view = move || {
        result().map(|_| {
            view! { <p>Led toggled!</p> }
        })
    };

    let on_click = move |_| {
        debug!("Toggle the led clicked");
        toggle_the_led_action.dispatch(());
    };

    view! {
        <div id="tool-toggle-led" class="tool">
            <h2>Toggle the LED</h2>
            <button type="button" on:click=on_click>
                Toggle the LED
            </button>
            <Show when=move || { !pending() } fallback=|| view! { <Loading/> }>
                <Show when=clicked>
                    <ErrorBoundary fallback=errors_fallback>{result_view}</ErrorBoundary>
                </Show>
            </Show>
        </div>
    }
}
