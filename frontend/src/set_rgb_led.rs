use common::{Empty, SetRgbLedParams};
use leptos::error::Result;
use leptos::html::Input;
use leptos::{
    component, create_action, create_node_ref, create_signal, view, web_sys, ErrorBoundary,
    IntoView, NodeRef, Show,
};
use web_sys::SubmitEvent;

use crate::utils::{errors_fallback, post_json, Error, Loading};

async fn set_rgb_led(r: u8, g: u8, b: u8) -> Result<Empty> {
    post_json("/api/set_rgb_led", &SetRgbLedParams { r, g, b }).await
}

#[component]
pub fn SetRgbLed() -> impl IntoView {
    let (clicked, set_clicked) = create_signal(false);
    let set_rgb_led_action = create_action(move |&(r, g, b): &(u8, u8, u8)| {
        set_clicked(true);
        set_rgb_led(r, g, b)
    });
    let pending = set_rgb_led_action.pending();
    let value = set_rgb_led_action.value();

    let result = move || value().unwrap_or(Err(Error::Unknown.into()));
    let result_view = move || {
        result().map(|_| {
            view! { <p>Led RGB set!</p> }
        })
    };

    let r_input_element: NodeRef<Input> = create_node_ref();
    let g_input_element: NodeRef<Input> = create_node_ref();
    let b_input_element: NodeRef<Input> = create_node_ref();
    let on_submit = move |ev: SubmitEvent| {
        // stop the page from reloading!
        ev.prevent_default();
        let r = r_input_element()
            .unwrap()
            .value()
            .parse()
            .unwrap_or_default();
        let g = g_input_element()
            .unwrap()
            .value()
            .parse()
            .unwrap_or_default();
        let b = b_input_element()
            .unwrap()
            .value()
            .parse()
            .unwrap_or_default();
        set_rgb_led_action.dispatch((r, g, b));
    };

    view! {
        <div id="tool-set-rgb-led" class="tool">
            <h2>Set RGB LED</h2>
            <form on:submit=on_submit>
                <label for="r_input">R:</label>
                <input
                    type="number"
                    id="r_input"
                    size="4"
                    value="0"
                    min="0"
                    max="255"
                    node_ref=r_input_element
                />
                <label for="g_input">G:</label>
                <input
                    type="number"
                    id="g_input"
                    size="4"
                    value="0"
                    min="0"
                    max="255"
                    node_ref=g_input_element
                />
                <label for="b_input">B:</label>
                <input
                    type="number"
                    id="b_input"
                    size="4"
                    value="0"
                    min="0"
                    max="255"
                    node_ref=b_input_element
                />
                <input type="submit" value="Submit"/>
            </form>
            <Show when=move || { !pending() } fallback=|| view! { <Loading/> }>
                <Show when=clicked>
                    <ErrorBoundary fallback=errors_fallback>{result_view}</ErrorBoundary>
                </Show>
            </Show>
        </div>
    }
}
