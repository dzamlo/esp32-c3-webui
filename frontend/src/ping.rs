use common::{PingAnswer, PingParams};
use leptos::error::Result;
use leptos::html::Input;
use leptos::{
    component, create_action, create_node_ref, create_signal, view, web_sys, ErrorBoundary,
    IntoView, NodeRef, Show,
};
use web_sys::SubmitEvent;

use crate::utils::{errors_fallback, post_json, Error, Loading};

async fn ping(ping_value: u32) -> Result<PingAnswer> {
    post_json("/api/ping?ping_value", &PingParams { ping_value }).await
}

#[component]
pub fn Ping() -> impl IntoView {
    let (clicked, set_clicked) = create_signal(false);
    let ping_action = create_action(move |ping_value: &u32| {
        set_clicked(true);
        ping(*ping_value)
    });
    let pending = ping_action.pending();
    let value = ping_action.value();

    let result = move || value().unwrap_or(Err(Error::Unknown.into()));
    let result_view = move || {
        result().map(|answer| {
            view! { <p>Pong value: {answer.pong_value}</p> }
        })
    };

    let input_element: NodeRef<Input> = create_node_ref();
    let on_submit = move |ev: SubmitEvent| {
        // stop the page from reloading!
        ev.prevent_default();
        let value: u32 = input_element().unwrap().value().parse().unwrap_or_default();
        ping_action.dispatch(value);
    };

    view! {
        <div id="tool-ping" class="tool">
            <h2>Ping</h2>
            <form on:submit=on_submit>
                <label for="ping_value_input">Ping value:</label>
                <input
                    type="number"
                    id="ping_value_input"
                    size="6"
                    value="1"
                    node_ref=input_element
                />
                <input type="submit" value="Submit"/>
            </form>
            <Show when=move || { !pending() } fallback=|| view! { <Loading/> }>
                <Show when=clicked>
                    <ErrorBoundary fallback=errors_fallback>{result_view}</ErrorBoundary>
                </Show>
            </Show>
        </div>
    }
}
