use common::ButtonEvent;
use futures_util::StreamExt;
use gloo_net::eventsource::futures::EventSource;
use gloo_timers::future::TimeoutFuture;
use leptos::{component, create_signal, spawn_local, view, IntoView};

#[component]
pub fn Button() -> impl IntoView {
    let (pressed, set_pressed) = create_signal(None);

    let pressed_text = move || match pressed() {
        Some(true) => "Pressed",
        Some(false) => "Released",
        None => "Unknown",
    };

    spawn_local(async move {
        loop {
            if let Ok(mut es) = EventSource::new("/api/button_events") {
                if let Ok(mut logs_stream) = es.subscribe("button_event") {
                    while let Some(Ok((_event_type, msg))) = logs_stream.next().await {
                        let button_event_str = msg.data().as_string().unwrap();
                        if let Ok(button_event) =
                            serde_json::from_str::<ButtonEvent>(&button_event_str)
                        {
                            set_pressed(Some(button_event.pressed));
                        } else {
                            set_pressed(None);
                        }
                    }
                }
            }
            set_pressed(None);
            // TODO: better rety timing stategy
            // Wait before retrying
            TimeoutFuture::new(1_000).await;
        }
    });

    view! {
        <div id="tool-ping" class="tool">
            <h2>Button</h2>
            <p>{pressed_text}</p>
        </div>
    }
}
