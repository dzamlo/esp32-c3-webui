#![warn(clippy::pedantic)]
#![allow(clippy::module_name_repetitions)]

use leptos::{component, view, IntoView};
use log::{info, warn};

use crate::button::Button;
use crate::gen_random::GenRandom;
use crate::ping::Ping;
use crate::read_adc::ReadAdc;
use crate::read_temperature_and_humidity::TemperatureAndHumidity;
use crate::set_rgb_led::SetRgbLed;
use crate::toggle_led::ToggleLed;

mod button;
mod gen_random;
mod ping;
mod read_adc;
mod read_temperature_and_humidity;
mod set_rgb_led;
mod toggle_led;
mod utils;

#[component]
fn App() -> impl IntoView {
    info!("App starting");

    view! {
        <div id="div-global">
            <h1>Embedded board control interface</h1>
            <div id="tool-container">
                <ToggleLed/>
                <Ping/>
                <ReadAdc/>
                <SetRgbLed/>
                <GenRandom/>
                <TemperatureAndHumidity/>
                <Button/>
            </div>
        </div>
    }
}

fn main() {
    _ = console_log::init_with_level(log::Level::Debug);
    console_error_panic_hook::set_once();

    leptos::mount_to_body(|| view! { <App/> });
}
