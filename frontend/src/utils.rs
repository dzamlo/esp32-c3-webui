use gloo_net::http::Request;
use leptos::error::Result;
use leptos::{component, view, CollectView, Errors, IntoView, RwSignal, SignalWith};
use serde::de::DeserializeOwned;
use serde::Serialize;
use thiserror::Error;

#[derive(Error, Debug)]
pub enum Error {
    #[error("server error ({status}): {text}")]
    ServerError { status: u16, text: String },
    #[error("unknow error")]
    Unknown,
}

pub fn errors_fallback(errors: RwSignal<Errors>) -> impl IntoView {
    let error_list = move || {
        errors.with(|errors| {
            errors
                .iter()
                .map(|(_, e)| view! { <li>{e.to_string()}</li> })
                .collect_view()
        })
    };

    view! {
        <div class="error">
            <h3>"Error"</h3>
            <ul>{error_list}</ul>
        </div>
    }
}

pub async fn post_json<T: Serialize, R: DeserializeOwned>(url: &str, params: &T) -> Result<R> {
    let response = Request::post(url).json(params)?.send().await?;
    if response.ok() {
        let answer = response.json::<R>().await?;
        Ok(answer)
    } else {
        Err(Error::ServerError {
            status: response.status(),
            text: response.text().await?,
        }
        .into())
    }
}

#[component]
pub fn Loading() -> impl IntoView {
    view! { <p>"Loading" <em class="loading-animation">"..."</em></p> }
}
