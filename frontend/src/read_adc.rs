use common::{ReadAdcAnswer, ReadAdcParams};
use leptos::error::Result;
use leptos::html::Input;
use leptos::{
    component, create_action, create_node_ref, create_signal, view, web_sys, ErrorBoundary,
    IntoView, NodeRef, Show,
};
use web_sys::SubmitEvent;

use crate::utils::{errors_fallback, post_json, Error, Loading};

async fn read_adc(adc_channel_index: u16) -> Result<ReadAdcAnswer> {
    post_json("/api/read_adc", &ReadAdcParams { adc_channel_index }).await
}

#[component]
pub fn ReadAdc() -> impl IntoView {
    let (clicked, set_clicked) = create_signal(false);
    let read_adc_action = create_action(move |&adc_channel_index: &u16| {
        set_clicked(true);
        read_adc(adc_channel_index)
    });
    let pending = read_adc_action.pending();
    let value = read_adc_action.value();

    let result = move || value().unwrap_or(Err(Error::Unknown.into()));
    let result_view = move || {
        result().map(|answer| {
            view! { <p>Adc value: {answer.adc_value}</p> }
        })
    };

    let input_element: NodeRef<Input> = create_node_ref();
    let on_submit = move |ev: SubmitEvent| {
        // stop the page from reloading!
        ev.prevent_default();
        let value = input_element().unwrap().value().parse().unwrap_or_default();
        read_adc_action.dispatch(value);
    };

    view! {
        <div id="tool-read-adc" class="tool">
            <h2>Read ADC</h2>
            <form on:submit=on_submit>
                <label for="adc_channel_index_input">Adc channel index:</label>
                <input
                    type="number"
                    id="adc_channel_index_input"
                    size="4"
                    value="0"
                    max="255"
                    node_ref=input_element
                />
                <input type="submit" value="Submit"/>
            </form>
            <Show when=move || { !pending() } fallback=|| view! { <Loading/> }>
                <Show when=clicked>
                    <ErrorBoundary fallback=errors_fallback>{result_view}</ErrorBoundary>
                </Show>
            </Show>
        </div>
    }
}
