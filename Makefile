.SUFFIXES:

BOARD_EXECUTABLE = board/target/riscv32imc-unknown-none-elf/release/board
DIST_DIR = frontend/dist
INDEX_FILE = $(DIST_DIR)/index.html
JS_FILE = $(DIST_DIR)/frontend.js
JS_GZ_FILE = $(DIST_DIR)/frontend.js.gz
WASM_FILE = $(DIST_DIR)/frontend_bg.wasm
WASM_GZ_FILE = $(WASM_FILE).gz
DIST_FILES = $(INDEX_FILE) $(JS_FILE) $(WASM_FILE)
INCLUDED_DIST_FILES = $(INDEX_FILE) $(JS_GZ_FILE) $(WASM_GZ_FILE)
BOARD_SRCS = $(shell find board/ -type f -a -not -path 'board/target*')
COMMON_SRCS = $(shell find common/ -type f -a -not -path 'common/target*')
FRONTEND_SRCS = $(shell find frontend/ -type f -a -not -path 'frontend/target*' -a -not -path 'frontend/dist/*')

.PHONY: run
run: $(BOARD_EXECUTABLE)
	espflash flash --monitor $(BOARD_EXECUTABLE)

.PHONY: clean
clean:
	cd common && cargo clean
	cd frontend && trunk clean && cargo clean
	cd board && cargo clean

.PHONY: clippy
clippy: $(INCLUDED_DIST_FILES)
	cd common && cargo clippy -- -D warnings
	cd frontend && cargo clippy -- -D warnings
	cd board && cargo clippy -- -D warnings

.PHONY: format
format:
	cd common && cargo fmt
	cd frontend && leptosfmt src && cargo fmt
	cd board && cargo fmt

.PHONY: format-check
format-check:
	cd common && cargo fmt --check
	cd frontend && leptosfmt --check src && cargo fmt --check
	cd board && cargo fmt --check

.PHONY: lint
lint: format-check clippy

.PHONY: instal-pre-commit-hook
instal-pre-commit-hook:
	cp scripts/pre-commit.sh .git/hooks/pre-commit
	chmod +x .git/hooks/pre-commit

$(BOARD_EXECUTABLE): $(INCLUDED_DIST_FILES) $(BOARD_SRCS) $(COMMON_SRCS)
	cd board && ESP_LOGLEVEL=info cargo build --release

$(DIST_FILES) &: $(FRONTEND_SRCS) $(COMMON_SRCS)
	cd frontend && trunk build

%.gz: %
	gzip --keep --no-name --best --force $<

